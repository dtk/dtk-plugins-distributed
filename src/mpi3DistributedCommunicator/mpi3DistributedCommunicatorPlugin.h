/* mpi3DistributedCommunicatorPlugin.h --- 
 * 
 * Author: Julien Wintz
 * Copyright (C) 2008-2011 - Julien Wintz, Inria.
 * Created: Tue Jan 15 16:52:52 2013 (+0100)
 * Version: $Id$
 * Last-Updated: Thu Feb 28 18:41:33 2013 (+0100)
 *           By: Julien Wintz
 *     Update #: 55
 */

/* Commentary: 
 * 
 */

/* Change log:
 * 
 */

#pragma once

#include <dtkDistributed/dtkDistributedCommunicator.h>

#include <QtCore>
#include <QtDebug>

class mpi3DistributedCommunicatorPlugin : public dtkDistributedCommunicatorPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkDistributedCommunicatorPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.dtkDistributedCommunicatorPlugin" FILE "mpi3DistributedCommunicatorPlugin.json")

public:
     mpi3DistributedCommunicatorPlugin(void) {}
    ~mpi3DistributedCommunicatorPlugin(void) {}

public:
    void   initialize(void);
    void uninitialize(void);
};
