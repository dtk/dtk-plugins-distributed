/* mpiDistributedCommunicatorPlugin.cpp --- 
 * 
 * Author: Julien Wintz
 * Copyright (C) 2008-2011 - Julien Wintz, Inria.
 * Created: Tue Jan 15 16:54:04 2013 (+0100)
 * Version: $Id$
 * Last-Updated: Mon Mar 25 11:57:26 2013 (+0100)
 *           By: Julien Wintz
 *     Update #: 63
 */

/* Commentary: 
 * 
 */

/* Change log:
 * 
 */

#include "mpiDistributedCommunicator.h"
#include "mpiDistributedCommunicatorPlugin.h"

#include <dtkDistributed/dtkDistributed.h>

// ///////////////////////////////////////////////////////////////////
// Helper functions
// ///////////////////////////////////////////////////////////////////

dtkDistributedCommunicator* mpiCreator(void);

// ///////////////////////////////////////////////////////////////////
// mpiDistributedCommunicatorPlugin
// ///////////////////////////////////////////////////////////////////

void mpiDistributedCommunicatorPlugin::initialize(void)
{
    dtkDistributed::communicator::pluginFactory().record("mpi", mpiCreator);
}

void mpiDistributedCommunicatorPlugin::uninitialize(void)
{
 
}

// ///////////////////////////////////////////////////////////////////
// Plugin meta data
// ///////////////////////////////////////////////////////////////////

DTK_DEFINE_PLUGIN(dtkDistributedCommunicator)

// ///////////////////////////////////////////////////////////////////
// Helper functions
// ///////////////////////////////////////////////////////////////////

dtkDistributedCommunicator *mpiCreator(void)
{
    return new mpiDistributedCommunicator();
}
