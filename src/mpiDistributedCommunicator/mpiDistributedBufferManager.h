// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkDistributed/dtkDistributedBufferManager.h>
#include <dtkDistributed/dtkDistributedCommunicator.h>

#include <dtkCore/dtkArray.h>

#include <QtCore>

#include <mpi.h>

// ///////////////////////////////////////////////////////////////////
// mpiDistributedBufferManagerPrivate
// ///////////////////////////////////////////////////////////////////

class mpiDistributedBufferManagerPrivate
{
public:
    dtkDistributedCommunicator *comm;
    int comm_size;
    int wid;

public:
    qlonglong object_size;
    MPI_Datatype object_mpi_type;
    char *buffer;
    QVarLengthArray<bool> rlocked;
    QVarLengthArray<bool> wlocked;

public:
    MPI_Win win;

public:
    MPI_Datatype mpiDataType(int metatype_id);
};

// ///////////////////////////////////////////////////////////////////

MPI_Datatype mpiDistributedBufferManagerPrivate::mpiDataType(int metatype_id)
{
    switch(metatype_id) {
    case QMetaType::Char:     return MPI_CHAR;
    case QMetaType::Int:      return MPI_INT;
    case QMetaType::Long:     return MPI_LONG;
    case QMetaType::LongLong: return MPI_LONG_LONG;
    case QMetaType::Float:    return MPI_FLOAT;
    case QMetaType::Double:   return MPI_DOUBLE;
    default:                  return MPI_BYTE;
    }
}

// ///////////////////////////////////////////////////////////////////
// mpiDistributedBufferManager
// ///////////////////////////////////////////////////////////////////

class mpiDistributedBufferManager : public dtkDistributedBufferManager
{
public:
     mpiDistributedBufferManager(dtkDistributedCommunicator *comm);
    ~mpiDistributedBufferManager(void);

protected:
    void *allocate(qlonglong objectSize, qlonglong capacity, int metatype_id);
    void  deallocate(void *buffer, qlonglong objectSize);

public:
    bool shouldCache(const qint32& owner);

public:
    void rlock(qlonglong wid);
    void rlock(void);
    void wlock(qlonglong wid);
    void wlock(void);

    void unlock(qlonglong wid);
    void unlock(void);

    bool locked(qlonglong wid);

public:
    void get(qint32 from, qlonglong position, void *array, qlonglong nelements = 1);
    void put(qint32 dest, qlonglong position, void *array, qlonglong nelements = 1);
    void addAssign(qint32 dest, qlonglong position, void *array, qlonglong count = 1);
    void subAssign(qint32 dest, qlonglong position, void *array, qlonglong count = 1);
    void mulAssign(qint32 dest, qlonglong position, void *array, qlonglong count = 1);
    void divAssign(qint32 dest, qlonglong position, void *array, qlonglong count = 1);
    bool compareAndSwap(qint32 dest, qlonglong position, void *array, void *compare);

public:
    mpiDistributedBufferManagerPrivate *d;
};

// ///////////////////////////////////////////////////////////////////

inline mpiDistributedBufferManager::mpiDistributedBufferManager(dtkDistributedCommunicator *comm) : d(new mpiDistributedBufferManagerPrivate)
{
    d->comm = comm;
    d->wid = comm->wid();
    d->comm_size = comm->size();

    d->rlocked.resize(d->comm_size);
    d->wlocked.resize(d->comm_size);

    d->object_size = 0;
    d->buffer = NULL;
    for(qlonglong i = 0; i < d->comm_size; ++i) {
        d->rlocked[i] = false;
        d->wlocked[i] = false;
    }
}

inline mpiDistributedBufferManager::~mpiDistributedBufferManager(void)
{
    if (d->buffer) {
        this->deallocate(d->buffer, d->object_size);
    }

    delete d;
    d = NULL;
}

inline void *mpiDistributedBufferManager::allocate(qlonglong objectSize, qlonglong capacity, int metatype_id)
{
    d->object_mpi_type = d->mpiDataType(metatype_id);
    if(capacity ==0)
        return NULL;

    d->object_size = objectSize;
    qlonglong buffer_size = capacity * objectSize;

    MPI_Alloc_mem(buffer_size, MPI_INFO_NULL, &d->buffer);
    MPI_Win_create(d->buffer, buffer_size, objectSize, MPI_INFO_NULL, MPI_COMM_WORLD, &(d->win));

    return d->buffer;
}

inline void mpiDistributedBufferManager::deallocate(void *buffer, qlonglong objectSize)
{
    Q_ASSERT(d->object_size == objectSize);
    Q_ASSERT(buffer == d->buffer);
    Q_UNUSED(objectSize);
    Q_UNUSED(buffer);

    if (!buffer)
        return;

    MPI_Free_mem(d->buffer);
    MPI_Win_free(&d->win);
    d->buffer = NULL;
}

inline bool mpiDistributedBufferManager::shouldCache(const qint32& owner)
{
    return true;
}

inline void mpiDistributedBufferManager::rlock(qlonglong wid)
{
    // we can't rlock mpi because each 'get' needs to be blocking, and
    // therefore requires a MPI_Lock/unlock to achieve the RMA
    // transfert
    /* MPI_Win_lock(MPI_LOCK_SHARED, wid,  MPI_MODE_NOCHECK, d->win); */

    d->rlocked[wid] = true;
}

inline void mpiDistributedBufferManager::rlock(void)
{
    for(qlonglong i = 0; i < d->comm_size; ++i) {
        d->rlocked[i] = true;
    }
}

inline void mpiDistributedBufferManager::wlock(qlonglong wid)
{
    MPI_Win_lock(MPI_LOCK_EXCLUSIVE, wid, 0, d->win);
    d->wlocked[wid] = true;
}

inline void mpiDistributedBufferManager::wlock(void)
{
    for(qlonglong i = 0; i < d->comm_size; ++i) {
        MPI_Win_lock(MPI_LOCK_EXCLUSIVE, i, 0, d->win);
        d->wlocked[i] = true;
    }
}

inline void mpiDistributedBufferManager::unlock(qlonglong wid)
{
    if (d->wlocked[wid])
        MPI_Win_unlock(wid, d->win);
    d->wlocked[wid] = false;
    d->rlocked[wid] = false;
}

inline void mpiDistributedBufferManager::unlock(void)
{
    for(qlonglong i = 0; i < d->comm_size; ++i) {
        if (d->wlocked[i])
            MPI_Win_unlock(i, d->win);
        d->wlocked[i] = false;
        d->rlocked[i] = false;
    }
}


inline bool mpiDistributedBufferManager::locked(qlonglong wid)
{
    return (d->wlocked[wid] || d->rlocked[wid]);
}

inline void mpiDistributedBufferManager::get(qint32 from, qlonglong position, void *array, qlonglong nelements)
{
    char *buffer = d->buffer;
    qlonglong array_size = d->object_size * nelements;
    bool locked = d->rlocked[from] || d->wlocked[from];

    if (from == d->wid) {
        if (!locked) {
            MPI_Win_lock(MPI_LOCK_SHARED, from,  0, d->win);
            memcpy(array, buffer + position * d->object_size, array_size);
            MPI_Win_unlock(from, d->win);
        } else {
            memcpy(array, buffer + position * d->object_size, array_size);
        }

    } else {
        MPI_Win_lock(MPI_LOCK_SHARED, from,  0, d->win);
        MPI_Get(array, array_size, MPI_BYTE, from, position, array_size, MPI_BYTE, d->win);
        MPI_Win_unlock(from, d->win);
    }
}

inline void mpiDistributedBufferManager::put(qint32 dest, qlonglong position, void *array, qlonglong nelements)
{
    char *buffer = d->buffer;
    qlonglong array_size = d->object_size * nelements;
    bool locked = d->wlocked[dest];

    if (dest == d->wid) {
        if (!locked) {
            MPI_Win_lock(MPI_LOCK_EXCLUSIVE, dest,  0, d->win);
            memcpy(buffer + position * d->object_size, array, array_size);
            MPI_Win_unlock(dest, d->win);
        } else {
            memcpy(buffer + position * d->object_size, array, array_size);
        }

    } else {
        if (!locked) {
            MPI_Win_lock(MPI_LOCK_EXCLUSIVE, dest,  0, d->win);
            MPI_Put(array, array_size, MPI_BYTE, dest, position, array_size, MPI_BYTE, d->win);
            MPI_Win_unlock(dest, d->win);
        } else {
            MPI_Put(array, array_size, MPI_BYTE, dest, position, array_size, MPI_BYTE, d->win);
        }
    }
}

inline void mpiDistributedBufferManager::addAssign(qint32 dest, qlonglong position, void *array, qlonglong nelements)
{
    Q_ASSERT((dest >= 0 || dest < d->comm->size()));

    char *buffer = d->buffer;
    bool locked = d->wlocked[dest];

    if (dest == d->wid) {
        if (!locked) {
            MPI_Win_lock(MPI_LOCK_EXCLUSIVE, dest, 0, d->win);
            MPI_Accumulate(array, nelements, d->object_mpi_type, dest, position, nelements, d->object_mpi_type, MPI_SUM, d->win);
            MPI_Win_unlock(dest, d->win);

        } else {
            this->operation_manager->addAssign(buffer + position * d->object_size, array, nelements);
        }
    } else {
        if (!locked) {
            MPI_Win_lock(MPI_LOCK_EXCLUSIVE, dest, 0, d->win);
            MPI_Accumulate(array, nelements, d->object_mpi_type, dest, position, nelements, d->object_mpi_type, MPI_SUM, d->win);
            MPI_Win_unlock(dest, d->win);

        } else {
            MPI_Accumulate(array, nelements, d->object_mpi_type, dest, position, nelements, d->object_mpi_type, MPI_SUM, d->win);
        }
    }
}

inline void mpiDistributedBufferManager::subAssign(qint32 dest, qlonglong position, void *array, qlonglong nelements)
{
    Q_ASSERT((dest >= 0 || dest < d->comm->size()));

    char *buffer = d->buffer;
    bool locked = d->wlocked[dest];

    if (dest == d->wid) {
        if (!locked) {
            void *sub_array = std::malloc(nelements * d->object_size);
            this->operation_manager->negate(sub_array, array, nelements);
            MPI_Win_lock(MPI_LOCK_EXCLUSIVE, dest, 0, d->win);
            MPI_Accumulate(sub_array, nelements, d->object_mpi_type, dest, position, nelements, d->object_mpi_type, MPI_SUM, d->win);
            MPI_Win_unlock(dest, d->win);
            std::free(sub_array);

        } else {
            this->operation_manager->subAssign(buffer + position * d->object_size, array, nelements);
        }
    } else {
        void *sub_array = std::malloc(nelements * d->object_size);
        this->operation_manager->negate(sub_array, array, nelements);
        if (!locked) {
            MPI_Win_lock(MPI_LOCK_EXCLUSIVE, dest, 0, d->win);
            MPI_Accumulate(sub_array, nelements, d->object_mpi_type, dest, position, nelements, d->object_mpi_type, MPI_SUM, d->win);
            MPI_Win_unlock(dest, d->win);

        } else {
            MPI_Accumulate(sub_array, nelements, d->object_mpi_type, dest, position, nelements, d->object_mpi_type, MPI_SUM, d->win);
        }
        std::free(sub_array);
    }
}

inline void mpiDistributedBufferManager::mulAssign(qint32 dest, qlonglong position, void *array, qlonglong nelements)
{
    Q_ASSERT((dest >= 0 || dest < d->comm->size()));

    char *buffer = d->buffer;
    bool locked = d->wlocked[dest];

    if (dest == d->wid) {
        if (!locked) {
            MPI_Win_lock(MPI_LOCK_EXCLUSIVE, dest, 0, d->win);
            MPI_Accumulate(array, nelements, d->object_mpi_type, dest, position, nelements, d->object_mpi_type, MPI_PROD, d->win);
            MPI_Win_unlock(dest, d->win);

        } else {
            this->operation_manager->mulAssign(buffer + position * d->object_size, array, nelements);
        }
    } else {
        if (!locked) {
            MPI_Win_lock(MPI_LOCK_EXCLUSIVE, dest, 0, d->win);
            MPI_Accumulate(array, nelements, d->object_mpi_type, dest, position, nelements, d->object_mpi_type, MPI_PROD, d->win);
            MPI_Win_unlock(dest, d->win);

        } else {
            MPI_Accumulate(array, nelements, d->object_mpi_type, dest, position, nelements, d->object_mpi_type, MPI_PROD, d->win);
        }
    }
}

inline void mpiDistributedBufferManager::divAssign(qint32 dest, qlonglong position, void *array, qlonglong nelements)
{
    Q_ASSERT((dest >= 0 || dest < d->comm->size()));

    char *buffer = d->buffer;
    bool locked = d->wlocked[dest];

    if (dest == d->wid) {
        if (!locked) {
            void *div_array = std::malloc(nelements * d->object_size);
            this->operation_manager->invert(div_array, array, nelements);
            MPI_Win_lock(MPI_LOCK_EXCLUSIVE, dest, 0, d->win);
            MPI_Accumulate(div_array, nelements, d->object_mpi_type, dest, position, nelements, d->object_mpi_type, MPI_PROD, d->win);
            MPI_Win_unlock(dest, d->win);
            std::free(div_array);

        } else {
            this->operation_manager->divAssign(buffer + position * d->object_size, array, nelements);
        }
    } else {
        void *div_array = std::malloc(nelements * d->object_size);
        this->operation_manager->invert(div_array, array, nelements);
        if (!locked) {
            MPI_Win_lock(MPI_LOCK_EXCLUSIVE, dest, 0, d->win);
            MPI_Accumulate(div_array, nelements, d->object_mpi_type, dest, position, nelements, d->object_mpi_type, MPI_PROD, d->win);
            MPI_Win_unlock(dest, d->win);

        } else {
            MPI_Accumulate(div_array, nelements, d->object_mpi_type, dest, position, nelements, d->object_mpi_type, MPI_PROD, d->win);
        }
        std::free(div_array);
    }
}

//FIXME: this is currently not working, use mpi3 instead.
inline bool mpiDistributedBufferManager::compareAndSwap(qint32 dest, qlonglong position, void *array, void *compare)
{
    Q_ASSERT((dest >= 0 || dest < d->comm->size()));

    char *buffer = d->buffer;
    bool locked = d->wlocked[dest];

    void * result ; //FIXME
    bool res;
    if (dest == d->wid) {
        if (!locked) {
            MPI_Win_lock(MPI_LOCK_EXCLUSIVE, dest, 0, d->win);
            res = this->operation_manager->compareAndSwap(buffer + position * d->object_size, array, compare);
            MPI_Win_unlock(dest, d->win);

        } else {
            res = this->operation_manager->compareAndSwap(buffer + position * d->object_size, array, compare);
        }
        return res;
    } else {
        result = std::malloc(d->object_size);
        if (!locked) {
            // since mpi_lock is not a real lock, this is probably not working ...
            MPI_Win_lock(MPI_LOCK_EXCLUSIVE, dest, 0, d->win);
            get(dest, position, result, 1);
            res = (memcmp(result, compare, d->object_size) == 0 );
            if (res) {
                put(dest, position, array, 1);
            }
            MPI_Win_unlock(dest, d->win);

        } else {

            get(dest, position, result, 1);
            res = (memcmp(result, compare, d->object_size) == 0 );
            if (res) {
                put(dest, position, array, 1);
            }

        }
        std::free(result);
        return (res);
    }
}

//
// mpiDistributedBufferManager.h ends here
