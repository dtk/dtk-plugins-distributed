/* @(#)mpiDistributedCommunicator.h ---
 *
 * Author: Nicolas Niclausse
 * Copyright (C) 2013 - Nicolas Niclausse, Inria.
 * Created: 2013/02/11 09:45:37
 */

/* Commentary:
 *
 */

/* Change log:
 *
 */

#pragma once

#include <dtkDistributed/dtkDistributedCommunicator.h>

class mpiDistributedCommunicatorPrivate;
class dtkDistributedBufferManager;
class mpiDistributedRequest;

class mpiDistributedCommunicator : public dtkDistributedCommunicator
{
    Q_OBJECT

public:
     mpiDistributedCommunicator(void);
    ~mpiDistributedCommunicator(void);

public:
    void   initialize(void);
    bool  initialized(void);
    void uninitialize(void);
    bool       active(void);

public:
    void   spawn(QStringList hostnames, QString wrapper, QMap<QString, QString> options);
    void    exec(QRunnable *work);
    void barrier(void);
    void unspawn(void);

public:
    dtkDistributedBufferManager *createBufferManager(void);
    void destroyBufferManager(dtkDistributedBufferManager *& buffer_manager);

public:
    void *allocate(qlonglong count, qlonglong size, qlonglong wid, qlonglong& buffer_id);

    void deallocate(qlonglong wid, const qlonglong& buffer_id);
    void rlock(qlonglong wid, const qlonglong& buffer_id);
    void wlock(qlonglong wid, const qlonglong& buffer_id);
    void unlock(qlonglong wid, const qlonglong& buffer_id);

public:
    void get(qint32 from, qlonglong position, void *array, qlonglong buffer_id, qlonglong nelements = 1);
    void put(qint32 dest, qlonglong position, void *array, qlonglong buffer_id, qlonglong nelements = 1);

public:
    void send(void *data, qint64 size, QMetaType::Type dataType, qint32 target, qint32 tag);
    void send(QByteArray& array, qint32 target, qint32 tag);

public:
    void broadcast(void *data, qint64 size, QMetaType::Type dataType, qint32 source);
    void broadcast(QByteArray &array, qint32 source);
    void broadcast(QVariant &v, qint32 source);

public:
    void receive(QByteArray &v,  qint32 source, qint32 tag);
    void receive(QByteArray &v,  qint32 source, qint32 tag, dtkDistributedCommunicatorStatus& status);
    void receive(void *data, qint64 size, QMetaType::Type dataType, qint32 source, qint32 tag, dtkDistributedCommunicatorStatus& status);
    void receive(void *data, qint64 size, QMetaType::Type dataType, qint32 source, qint32 tag);
    using dtkDistributedCommunicator::receive;

public:
    dtkDistributedRequest *ireceive(void   *data, qint64 size, QMetaType::Type dataType, qint32 source, int tag);
    void wait(dtkDistributedRequest *req);

public:
    void reduce(void *send, void *recv, qint64 size, QMetaType::Type dataType, OperationType operationType, qint32 target, bool all);
    void gather(void *send, void *recv, qint64 size, QMetaType::Type dataType, qint32 target, bool all);

public:
    qint32  wid(void);
    qint32 size(void);
    void*  data(void);

public:
    mpiDistributedCommunicatorPrivate *d;

};



