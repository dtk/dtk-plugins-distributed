### CMakeLists.txt ---
##
## Author: Julien Wintz
## Copyright (C) 2008-2011 - Julien Wintz, Inria.
## Created: Tue Jan 15 16:51:15 2013 (+0100)
######################################################################
##
### Commentary:
##
######################################################################
##
### Change log:
##
######################################################################

project(mpiDistributedCommunicator)

## ###################################################################
## Build tree setup
## ###################################################################

set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/plugins)
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/plugins)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/plugins)

## ###################################################################
## Dependencies
## ###################################################################

find_package(MPI)

mark_as_advanced(MPI_EXTRA_LIBRARY)
mark_as_advanced(MPI_LIBRARY)

if(MPI_FOUND)
  include_directories(${MPI_INCLUDE_PATH})
  set(COMPILE_FLAGS ${COMPILE_FLAGS} ${MPI_COMPILE_FLAGS})
endif(MPI_FOUND)

## ###################################################################
## Build setup
## ###################################################################

set(CMAKE_AUTOMOC ON)
set(CMAKE_INCLUDE_CURRENT_DIR ON)

## ###################################################################
## Build rules
## ###################################################################

add_definitions(-DQT_PLUGIN)

add_library(${PROJECT_NAME} SHARED
  mpiDistributedBufferManager.h
  mpiDistributedCommunicator.h
  mpiDistributedCommunicator.cpp
  mpiDistributedCommunicatorPlugin.h
  mpiDistributedCommunicatorPlugin.cpp
  )

## ###################################################################
## Link rules
## ###################################################################

target_link_libraries(${PROJECT_NAME} dtkDistributed)
target_link_libraries(${PROJECT_NAME} dtkLog)
target_link_libraries(${PROJECT_NAME} ${MPI_LIBRARIES})

qt5_use_modules(${PROJECT_NAME} Core)
qt5_use_modules(${PROJECT_NAME} Network)
qt5_use_modules(${PROJECT_NAME} Xml)

## #################################################################
## Install rules
## #################################################################


install(TARGETS ${PROJECT_NAME}
  RUNTIME DESTINATION plugins/${DTK_CURRENT_LAYER}
  LIBRARY DESTINATION plugins/${DTK_CURRENT_LAYER}
  ARCHIVE DESTINATION plugins/${DTK_CURRENT_LAYER})

